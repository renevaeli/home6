/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomGraph(6, 9);

      System.out.println ("Graph g: " + g);

      //remove first vertex
      g.removeVertex("v1");
      System.out.println("Remove first vertex v1 from g: " + g);

      //remove middle vertex
      g.removeVertex("v3");
      System.out.println("Remove middle vertex v3 from g: " + g);

      //remove last vertex
      g.removeVertex("v6");
      System.out.println("Remove last vertex v6 from g: " + g);

      //input nonexistent vertex id
      g.removeVertex("123");
      System.out.println("Remove nonexistent vertex 123 from g: " + g);

      //try removing from empty graph
      Graph g2 = new Graph ("G");
      g.createRandomGraph(0, 0);
      System.out.println ("Empty graph g2: " + g2);
      g2.removeVertex("v1");
      System.out.println("Try removing v1 from empty graph g2: " + g2);

      //try removing from huge graph
      Graph g3 = new Graph ("G");
      g3.createRandomGraph(2400, 3600);
      System.out.println ("Huge graph: " + g3);
      g3.removeVertex("v1000");
      System.out.println("Try removing v1000 from huge graph g3: " + g3);


      // TODO!!! Your experiments here
   }

   // TODO!!! add javadoc relevant to your problem
   class Vertex {

      private String vertexID;
      private Vertex nextVertex;
      private Arc firstArc;
      private int info = 0;
      // You can add more fields, if needed

      Vertex (String id, Vertex next, Arc first) {
         vertexID = id;
         nextVertex = next;
         firstArc = first;
         //System.out.println("Vertex modified: " + this.longToString());
      }

      Vertex (String id) {
         this (id, null, null);
      }

      @Override
      public String toString() {
         return vertexID;
      }

      public String longToString() {
         return "id: " + vertexID + ", nextV: " + nextVertex + ", firstA: " + firstArc;
      }

   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String arcID;
      private Vertex targetVertex;
      private Arc nextArc;
      private int info = 0;
      // You can add more fields, if needed

      Arc (String id, Vertex target, Arc next) {
         arcID = id;
         targetVertex = target;
         nextArc = next;
      }

      Arc (String id) {
         this (id, null, null);
      }

      @Override
      public String toString() {
         return arcID;
      }

      public String longToString() {
         return "id: " + arcID + ", targetV: " + targetVertex + ", nextA: " + nextArc;
      }

   } 


   class Graph {

      private String graphID;
      private Vertex graphFirstVertex;
      private int info = 0;
      // You can add more fields, if needed

      Graph (String id, Vertex first) {
         graphID = id;
         graphFirstVertex = first;
         //System.out.println("Graph: " + id + ", " + first);
      }

      Graph (String id) {
         this (id, null);
      }

      @Override
      public String toString() {
         String newLine = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (newLine);
         sb.append (graphID);
         sb.append (newLine);
         Vertex v = graphFirstVertex;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.firstArc;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.targetVertex.toString());
               sb.append (")");
               a = a.nextArc;
            }
            sb.append (newLine);
            v = v.nextVertex;
         }
         //System.out.println("sb.toString: " + sb.toString());
         return sb.toString();
      }

      public Vertex createVertex (String vId) {
         Vertex res = new Vertex (vId);
         //System.out.println("vertex created: " + res.longToString());
         res.nextVertex = graphFirstVertex;
         //System.out.println("res next set: " + res.longToString());
         graphFirstVertex = res;

         //System.out.println("vertex done: " + res.longToString());
         return res;
      }

      public Arc createArc (String aId, Vertex vFrom, Vertex vTo) {
         Arc res = new Arc (aId);

         //System.out.println("arc created: " + res.longToString());
         res.nextArc = vFrom.firstArc;
         vFrom.firstArc = res;
         res.targetVertex = vTo;

         //System.out.println("vFrom firstarc set: " + vFrom.longToString());
         //System.out.println("arc done: " + res.longToString());
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = graphFirstVertex;

         while (v != null) {
            v.info = info++;
            v = v.nextVertex;
         }

         int[][] res = new int [info][info];
         v = graphFirstVertex;

         while (v != null) {
            int i = v.info;
            Arc a = v.firstArc;

            while (a != null) {
               int j = a.targetVertex.info;
               res [i][j]++;
               a = a.nextArc;
            }
            v = v.nextVertex;
         }
         /*
         for (int i = 0; i < info; i++) {
            for (int j = 0; j < info; j++) {
               System.out.print(res[i][j] + " ");
            }
            System.out.print("\n");
         }
         */
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Impossible number of edges: " + m);

         graphFirstVertex = null;

         //System.out.println("\n*** Creating random tree ***");
         createRandomTree (n);       // n-1 edges created here

         //System.out.println("\n*** Creating empty vertex array ***");
         Vertex[] vert = new Vertex [n];
         Vertex v = graphFirstVertex;
         //System.out.println("GraphFirstVertex: " + v);
         int c = 0;

         //System.out.println("\n*** Populating vertex array ***");
         while (v != null) {
            //System.out.println("Vertex in randomsimplegraph: " + v.longToString());
            vert[c++] = v;
            v = v.nextVertex;
         }

         //System.out.println("\n*** Creating adjacency matrix ***");
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges

         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random targetVertex
            /*
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges
            */
            Vertex vi = vert [i];
            Vertex vj = vert [j];

            //System.out.println("\n*** Creating new arcs ij and ji ***");
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      public void removeVertex(String vid) {

         Vertex v = graphFirstVertex;

         //remove vertex if first
         if (v != null) {
            if (v.vertexID.equals(vid)) {
               graphFirstVertex = graphFirstVertex.nextVertex;
               v = v.nextVertex;
            }
         }

         while (v != null) {

            //remove vertex if further (outgoing arcs are automatically removed)
            if (v.nextVertex != null) {
               if (v.nextVertex.vertexID.equals(vid)) {
                  v.nextVertex = v.nextVertex.nextVertex;
                  continue;
               }
            }

            //Remove arc if first
            Arc a = v.firstArc;
            if (a != null) {
               if (a.targetVertex.vertexID.equals(vid)) {
                  v.firstArc = a.nextArc;
                  a = a.nextArc;
               }
            }

            //Remove arc if further
            while (a != null) {
               if (a.nextArc != null) {
                  if (a.nextArc.targetVertex.vertexID.equals(vid)) {
                     a.nextArc = a.nextArc.nextArc;
                     continue;
                  }
               }
               a = a.nextArc;
            }
            v = v.nextVertex;
         }
      }

   }

} 

